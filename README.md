# Test Kipsoft


Le choix du langage est libre.

Api : https://entreprise.data.gouv.fr/api/sirene/v1/full_text/XXXX?per_page=5&page=1

ou XXXX est le terme de recherche par exemple

https://entreprise.data.gouv.fr/api/sirene/v1/full_text/kipsoft?per_page=5&page=1

Objectif : Exploiter cette api via une interface web

Durée approximative : 3h

0/ - Versioner le code avec git

1/ - Faire une interface simple mais propre avec un formulaire de recherche (pour le css utiliser ( https://getbootstrap.com/ ou un autre framework css de votre choix)

2/ - Afficher la liste des résultats

3/ - Afficher la fiche de l'entreprise via un bouton

4/ - Bonus : Mettre en place la pagination

6/ - Bonus : La qualité du code sera également évaluée !