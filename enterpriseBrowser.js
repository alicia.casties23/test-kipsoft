class App extends React.Component {
    constructor(props) {
        super(props);
        this.handler = this.handler.bind(this);
        this.state = {
            enterprises: null,
        };
    }

    async handler(enterprise) {
        axios.get('https://entreprise.data.gouv.fr/api/sirene/v1/full_text/'+ enterprise+'?per_page=10&page=1')
            .then(res => {
                this.setState({enterprises: res.data.etablissement});
            });
    }

    render() {
        let enterprises;
        let enterprisesList = [];
        if (this.state.enterprises !== null) {
            enterprises = this.state.enterprises.map(enterprise => {
                enterprises++;
                return (
                    <div key={btoa(enterprise.nom_raison_sociale)} class="list-group">
                        <Enterprise name={enterprise.nom_raison_sociale}
                                    address={enterprise.geo_adresse}
                                    juridicalNature={enterprise.libelle_nature_juridique_entreprise}
                                    wording={enterprise.libelle_activite_principale_entreprise}
                        />
                    </div>
                );
            });
            for (let i = 0; i < enterprises.length; i++) {
                enterprisesList.push(
                    <div id="enterprise" key={i} children={enterprises[i]}/>
                );
            }
        }
        return (
            <div >
                <Header handler={this.handler}/>
                <div >
                    {this.state.enterprises !== null ? enterprisesList : enterprises}
                </div>
                <Footer change_page={this.change_page} />
            </div>
        );
    }
}

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            value: ''
        };
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        this.props.handler(this.state.value);
        event.preventDefault();
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">Enterprise Browser</a>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <form className="form-inline my-2 my-lg-0"  onSubmit={this.handleSubmit}>
                        <input onChange={this.handleChange} className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
                        <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
        );
    }
}

class Enterprise extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            address: props.address,
            name: props.name,
            juridicalNature: props.juridicalNature,
            wording: props.wording
        }
    }

    render() {
        return (
            <div className="list-group-item list-group-item-action" role="group">
                <button id="btnGroupDrop1" type="button" className="btn btn-light dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{this.state.name}
                </button>
                <div className="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a className="dropdown-item" href="#"> {this.state.address}</a>
                    <a className="dropdown-item" href="#"> {this.state.juridicalNature}</a>
                    <a className="dropdown-item" href="#"> {this.state.wording}</a>
                </div>
            </div>
        );
    }
}

class Footer extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>

                <ul className="pagination justify-content-center">
                    <li className="page-item disabled">
                        <a className="page-link"  tabIndex="-1">Previous</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link">Next</a>
                    </li>
                </ul>
            </div>
        );
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById("App")
);

